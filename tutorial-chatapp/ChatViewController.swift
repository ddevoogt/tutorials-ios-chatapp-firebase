//
//  ChatViewController.swift
//  tutorial-chatapp
//
//  Created by Douglas De Voogt on 9/1/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var selectedUser: User!
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var userInput: UITextField!
    var cellHeight = 44
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.estimatedRowHeight = 88.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        PostManager.fillPosts(uid: FirebaseManager.currentUser?.uid, toId: (selectedUser?.uid)!) {
            (result: String) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        PostManager.posts = []
    }
    
    @IBAction func sendButton_click(_ sender: UIButton) {
        PostManager.addPost(username: selectedUser.username, text: userInput.text!, toId: selectedUser.uid)
        userInput.text = ""
    }
    // MARK: - Table view data source.
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return PostManager.posts.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChatTableViewCell
        
        let messageText = cell.messageText!
        messageText.delegate = self
        cellHeight = Int(messageText.contentSize.height)
        let post = PostManager.posts[indexPath.row]
        cell.messageText.text = post.text
        
        return cell
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ChatViewController: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        let currentOffset = tableView.contentOffset
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
        tableView.setContentOffset(currentOffset, animated: false)
    }
}
