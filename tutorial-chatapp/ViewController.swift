//
//  ViewController.swift
//  tutorial-chatapp
//
//  Created by Douglas De Voogt on 9/1/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var username: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var loginButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        RemoteConfigManager.remoteConfigInit(firstControl: self.loginButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginButton_click(_ sender: Any) {
//        let username = "dcd.xactware+chat1@gmail.com"
//        let password = "Pass1234"
        FirebaseManager.Login(email: email.text!, password: password.text!, completion: { (success) in
            if success {
                self.performSegue(withIdentifier: "showProfile", sender: sender)
            }
        })
        
    }
    
    @IBAction func createAccountButton_click(_ sender: UIButton) {
        FirebaseManager.CreateAccount(email: email.text!, password: password.text!, username: username.text!) { (result) in
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "showProfile", sender: sender)
            }
        }
    }
    
}

