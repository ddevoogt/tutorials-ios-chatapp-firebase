//
//  ChatTableViewCell.swift
//  tutorial-chatapp
//
//  Created by Douglas De Voogt on 9/5/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {
    
    
    @IBOutlet var messageText: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        messageText.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
