//
//  RemoteConfigManager.swift
//  tutorial-chatapp
//
//  Created by Douglas De Voogt on 9/5/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import UIKit
import FirebaseRemoteConfig

class RemoteConfigManager: NSObject {
    
    static let interval: TimeInterval = 10
    static var remoteConfigValues = [String:String]()
    static var controlsToUpdate = [String:NSObject]()
    
    static func remoteConfigInit(firstControl:UIButton){
        
        controlsToUpdate["loginButton"] = firstControl
        
        let remoteValues = RemoteConfig.remoteConfig()
        remoteConfigValues["loginButton"] = remoteValues["loginButton"].stringValue
        remoteConfigValues["photoButtonUpdate"] = remoteValues["photoButtonUpdate"].stringValue
        
        let remoteConfigDefaults = [
            "loginButton": "login" as NSObject,
            "photoButtonUpdate": "update" as NSObject
        ]
        
        RemoteConfig.remoteConfig().setDefaults(remoteConfigDefaults)
        
        //43200 = 12 hours
        Timer.scheduledTimer(timeInterval: RemoteConfigManager.interval, target: self, selector: #selector(RemoteConfigManager.startFetching(firstControl:)), userInfo: nil, repeats: true)
        
        
        remoteConfigDebugMode()
        startFetching(firstControl: firstControl)
        
    }
    
    static func startFetching(firstControl: UIButton) {
        
        let remoteValues = RemoteConfig.remoteConfig()
        remoteConfigValues["loginButton"] = remoteValues["loginButton"].stringValue
        remoteConfigValues["photoButtonUpdate"] = remoteValues["photoButtonUpdate"].stringValue
        
        
        RemoteConfig.remoteConfig().fetch(withExpirationDuration: RemoteConfigManager.interval) { (status, error) in
            guard error == nil else{
                print("Error fetching remote config values \(error!)")
                return
            }
            
            RemoteConfig.remoteConfig().activateFetched()
            
            print("loginButting value: \(remoteConfigValues["loginButton"]!)")
            let fc = controlsToUpdate["loginButton"] as! UIButton
            fc.setTitle(remoteConfigValues["loginButton"], for: .normal)
        }
    }
    
    static func remoteConfigDebugMode() {
        let debugSettings = RemoteConfigSettings(developerModeEnabled: true)
        RemoteConfig.remoteConfig().configSettings = debugSettings!
    }
}
