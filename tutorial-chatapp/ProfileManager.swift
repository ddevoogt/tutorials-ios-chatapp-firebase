//
//  ProfileManager.swift
//  tutorial-chatapp
//
//  Created by Douglas De Voogt on 9/5/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class ProfileManager: NSObject {
    
    static let databaseRef = Database.database().reference()
    static let uid = Auth.auth().currentUser?.uid
    
    static var users = [User]()
    
    static func getCurrentUser(uid: String = "") -> User? {
        let uidToUse = uid == "" ? self.uid : uid
        
        if let idx = users.index(where: {$0.uid == uidToUse}){
            return users[idx]
        }
        
        return nil
    }
    
    static func fillUsers(completion: @escaping () -> Void) {
        databaseRef.child("users").observe(.childAdded, with: {
            (snapshot) in
            print(snapshot)
            
            if let result = snapshot.value as? [String:AnyObject]{
                let uid = result["uid"]! as! String
                let username = result["username"]! as! String
                let email = result["email"]! as! String
                let profileImageUrl = result["profileImageUrl"]! as! String
                
                let u = User(uid: uid, username: username, email: email, profileImageUrl: profileImageUrl)
                
                self.users.append(u)
            }
            completion()
        })
    }

}
