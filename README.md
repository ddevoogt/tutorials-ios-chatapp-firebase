# README #

### Creating a Cloud-based Swift iOS Chat App with Firebase
by Brett Romero

Firebase has become one of the most popular cloud based databases. This course will teach you how to build a Swift iOS chat app by utilizing Firebase.


### Pluralsight
https://app.pluralsight.com/library/courses/creating-cloud-based-swift-ios-chat-app-firebase/table-of-contents

### Cool things in this app:

* Login view and calls
* Multiple views 
* Navigation


### Next Module: 
https://app.pluralsight.com/player?course=creating-cloud-based-swift-ios-chat-app-firebase&author=brett-romero&name=creating-cloud-based-swift-ios-chat-app-firebase-m8&clip=3&mode=live
